package investing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Properties;

public class Client {
	
	/*
	 * This class handles interactions from each page with the server, In order to receive the required information.
	 * The client will send for the information, receive a response from the server, and relay this back to the specific page.
	 */
	
	public static String[] response = new String[1];
	
	 private String socketNumber;
	 
	    public Client (String socketNumber) {
	    	this.socketNumber = socketNumber;
	    }
	    

	public void runMainPage() throws UnknownHostException, IOException {
		
		Socket s = new Socket("localhost", 5001);
		PrintWriter p = new PrintWriter(s.getOutputStream());
		
		InputStreamReader i = new InputStreamReader(s.getInputStream());
		BufferedReader br = new BufferedReader(i);
		
		String str = br.readLine();
		System.out.println("server: " + str); 
		
		MainPage mp = new MainPage();
	    mp.setVisible(true);
	}
	
	public void register() throws UnknownHostException, IOException {

		Socket s = new Socket("localhost", 6000);
		PrintWriter p = new PrintWriter(s.getOutputStream());
		p.println(RegistrationPage.details[0]);
		p.flush();
		p.println(RegistrationPage.details[1]);
		p.flush();
		p.println(RegistrationPage.details[2]);
		p.flush();
		p.println(RegistrationPage.details[3]);
		p.flush();
		
		InputStreamReader i = new InputStreamReader(s.getInputStream());
		BufferedReader br = new BufferedReader(i);
		String str = br.readLine();
		
		if (str.contains("failed")) {
			RegistrationPage.setOutcome("failed");
		} else if (str.contains("success")) {
			RegistrationPage.setOutcome("success");
		}
		s.close();
		
	}
	
	public void login() throws UnknownHostException, IOException, InterruptedException {
	
		Socket s = new Socket("localhost", 5002);
		PrintWriter p = new PrintWriter(s.getOutputStream());
		p.println(LoginPage.details[0]);
		p.flush();
		p.println(LoginPage.details[1]);
		p.flush();
		
		InputStreamReader i = new InputStreamReader(s.getInputStream());
		BufferedReader br = new BufferedReader(i);
		String str = br.readLine();
		
		if (str.contains("success")) {
			LoginPage.setAnswer("login successful!");		
		} else if (str.contains("failed")) {
			LoginPage.setAnswer("login unsuccessful!"); 
			s.close();
		}
		
		
	}
	
	
	public void storeDetails() throws UnknownHostException, IOException, InterruptedException {
		
		Socket s = new Socket("localhost", 5005);
		PrintWriter p = new PrintWriter(s.getOutputStream());
		p.println(MainPage.userInputs[0]);
		p.flush();
		p.println(MainPage.userInputs[1]);
		p.flush();
		p.println(MainPage.userInputs[2]);
		p.flush();
		p.println(MainPage.scrollerInputs[0]);
		p.flush();
		p.println(MainPage.scrollerInputs[1]);
		p.flush();
		p.println(MainPage.scrollerInputs[2]);
		p.flush();
		p.println(MainPage.scrollerInputs[3]);
		p.flush();
		p.println(MainPage.userInputs[3]);
		p.flush();
		p.println(MainPage.userInputs[4]);
		p.flush();
		p.println(MainPage.userInputs[5]);
		p.flush();
		p.println(MainPage.userInputs[6]);
		p.flush();
		
		s.close();
		
		
	}
	
	public void filterResults() throws UnknownHostException, IOException, InterruptedException {
		
		Socket s = new Socket("localhost", 5006);
		PrintWriter p = new PrintWriter(s.getOutputStream());
		
		while(DatabasePage.filter.size() > 0) {
			p.println(DatabasePage.filter.get(0));
			p.flush();
			DatabasePage.filter.remove(0);
		}
		
		p.println("stop");
		p.flush();
	
		
		InputStreamReader i = new InputStreamReader(s.getInputStream());
		BufferedReader br = new BufferedReader(i);
		
		int x = 1;
		
		while (x == 1) {
			String str = br.readLine();
			if (str.isEmpty()) {
				
			} else if(str.contains("All results found")) {
				x = 2;	
			} else {
				if (DatabasePage.dates.size() == DatabasePage.sScore.size()) {
					DatabasePage.dates.add(str);
				} else if (DatabasePage.name.size() == DatabasePage.sScore.size()) {
					DatabasePage.name.add(str);
				} else if (DatabasePage.size.size() == DatabasePage.sScore.size()) {
					DatabasePage.size.add(str);
				} else if (DatabasePage.fc.size() == DatabasePage.sScore.size()) {
					DatabasePage.fc.add(str);
				} else if (DatabasePage.es.size() == DatabasePage.sScore.size()) {
					DatabasePage.es.add(str);
				} else if (DatabasePage.dr.size() == DatabasePage.sScore.size()) {
					DatabasePage.dr.add(str);
				} else if (DatabasePage.eg.size() == DatabasePage.sScore.size()) {
					DatabasePage.eg.add(str);
				} else if (DatabasePage.pe.size() == DatabasePage.sScore.size()) {
					DatabasePage.pe.add(str);
				} else if (DatabasePage.pb.size() == DatabasePage.sScore.size()) {
					DatabasePage.pb.add(str);
				} else {
					DatabasePage.sScore.add(str);
				}
				
			}	
			}
		
		s.close();
		
	}
	
public void establishConnection() throws UnknownHostException, IOException {
		
		Socket s = new Socket("localhost", 5006);
		PrintWriter p = new PrintWriter(s.getOutputStream());
		
		InputStreamReader i = new InputStreamReader(s.getInputStream());
		BufferedReader br = new BufferedReader(i);
		
		String str = br.readLine();
		System.out.println("server: " + str); 
		
	}
 	
	    
}
