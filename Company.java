package regression;

public class Company {
	
	private String name;
	private int size;
	private int financialCondition;
	private int earningStability;
	private int dividendRecord;
	private double earningsGrowth;
	private double pepb;
	private double laterEarningsGrowth;
	private double priceChange;
	private int response;
	
	
	public Company (String name, int size, int financialCondition, int earningStability, int dividendRecord, double earningsGrowth, double pepb, double laterEarningsGrowth, double priceChange, int response) {
		this.name = name;
		this.size = size;
		this.financialCondition = financialCondition;
		this.earningStability = earningStability;
		this.dividendRecord = dividendRecord;
		this.earningsGrowth = earningsGrowth;
		this.pepb = pepb;
		this.laterEarningsGrowth = laterEarningsGrowth;
		this.priceChange = priceChange;
		this.response = response;
	}



	public int getResponse() {
		return response;
	}



	public void setResponse(int response) {
		this.response = response;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public int getSize() {
		return size;
	}



	public void setSize(int size) {
		this.size = size;
	}



	public int getFinancialCondition() {
		return financialCondition;
	}



	public void setFinancialCondition(int financialCondition) {
		this.financialCondition = financialCondition;
	}



	public int getEarningStability() {
		return earningStability;
	}



	public void setEarningStability(int earningStability) {
		this.earningStability = earningStability;
	}



	public int getDividendRecord() {
		return dividendRecord;
	}



	public void setDividendRecord(int dividendRecord) {
		this.dividendRecord = dividendRecord;
	}



	public double getEarningsGrowth() {
		return earningsGrowth;
	}



	public void setEarningsGrowth(double earningsGrowth) {
		this.earningsGrowth = earningsGrowth;
	}



	public double getPepb() {
		return pepb;
	}



	public void setPepb(double pepb) {
		this.pepb = pepb;
	}



	public double getLaterEarningsGrowth() {
		return laterEarningsGrowth;
	}



	public void setLaterEarningsGrowth(double laterEarningsGrowth) {
		this.laterEarningsGrowth = laterEarningsGrowth;
	}



	public double getPriceChange() {
		return priceChange;
	}



	public void setPriceChange(double priceChange) {
		this.priceChange = priceChange;
	}



	@Override
	public String toString() {
		return "Company [name=" + name + ", size=" + size + ", financialCondition=" + financialCondition
				+ ", earningStability=" + earningStability + ", dividendRecord=" + dividendRecord + ", earningsGrowth="
				+ earningsGrowth + ", pe*pb=" + pepb + ", laterEarningsGrowth=" + laterEarningsGrowth + "% yoy over 5 years, priceChange="
				+ priceChange + "% growth after 5 years]";
	}
	
	
	
}
