package investing;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Date;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.awt.Cursor;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import java.awt.List;
import java.awt.Scrollbar;
import javax.swing.DefaultComboBoxModel;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JEditorPane;
import javax.swing.DropMode;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class DatabasePage extends JFrame {

	private JPanel contentPane;
	private JTextField companyNameField;
	public static String[] details = new String[4];
	private JTextField earningsGrowthInput;
	private JTextField PEInput;
	private JTextField PBInput;
	public static String user = "rs50";
	public static int[] scrollerInputs;
	public static String[] userInputs;
	public static ArrayList<String> filter = new ArrayList();
	public static ArrayList<String> resultSet = new ArrayList();
	public static ArrayList <String> dates = new ArrayList();
	public static ArrayList <String> name = new ArrayList();
	public static ArrayList <String> size = new ArrayList();
	public static ArrayList <String> fc = new ArrayList();
	public static ArrayList <String> es = new ArrayList();
	public static ArrayList <String> dr = new ArrayList();
	public static ArrayList <String> eg = new ArrayList();
	public static ArrayList <String> pe = new ArrayList();
	public static ArrayList <String> pb = new ArrayList();
	public static ArrayList <String> sScore = new ArrayList();
	private JTextField dividendRecordField;
	private JTextField earningStabilityField;
	private JTextField financialConditionField;
	private JTextField sizeField;
	private JTextField dateField;
	private JTextField sScoreField;
	private JTable table;
	private JTable table_1;
	/**
	 * Launch the application.
	 */
	
	public static void main (String[] args) throws IOException {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
					
				try {
					DatabasePage frame = new DatabasePage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}
	
	

	/**
	 * Create the frame.
	 */
	public DatabasePage() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(560, 200, 1696, 936); // frame size and position.
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 204));
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	
		
		JLabel registrationPageLink = new JLabel("Click here to return to the home page");
		registrationPageLink.setFont(new Font("Lucida Grande", Font.PLAIN, 23));
		registrationPageLink.setForeground(new Color(255, 255, 153));
		registrationPageLink.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e){
				 MainPage mp = new MainPage();
		         mp.setVisible(true);
				 dispose();
			}
		});
		
		
		Date currentDate = new Date();
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = timeFormat.format(currentDate);
		JLabel dateLabel = new JLabel(date);
		dateLabel.setFont(new Font("Lucida Grande", Font.BOLD, 19));
		dateLabel.setForeground(new Color(0, 0, 205));
		dateLabel.setBounds(6, 6, 194, 40);
		contentPane.add(dateLabel);
		
		JLabel label = new JLabel("");
		label.setOpaque(true);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(new Color(0, 0, 204));
		label.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		label.setBackground(new Color(255, 255, 153));
		label.setBounds(636, 118, 19, 796);
		contentPane.add(label);
		
		JLabel lblHistory = new JLabel("HISTORY");
		lblHistory.setFont(new Font("Lucida Grande", Font.BOLD, 33));
		lblHistory.setForeground(new Color(255, 255, 153));
		lblHistory.setBounds(6, 124, 217, 56);
		contentPane.add(lblHistory);
		
		JLabel lblFilterBelow = new JLabel("Please adjust the filters below:");
		lblFilterBelow.setForeground(new Color(255, 255, 153));
		lblFilterBelow.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblFilterBelow.setBounds(6, 184, 374, 47);
		contentPane.add(lblFilterBelow);
		
		JLabel lblUsername = new JLabel("Logged in as: " + user);
		lblUsername.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsername.setFont(new Font("Lucida Grande", Font.BOLD, 19));
		lblUsername.setForeground(new Color(0, 0, 205));
		lblUsername.setBounds(1226, 6, 449, 40);
		contentPane.add(lblUsername);
		
		JLabel email = new JLabel("Name of Company:");
		email.setForeground(new Color(255, 255, 153));
		email.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		email.setBounds (45, 337, 262, 56);
		contentPane.add(email);
		registrationPageLink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		registrationPageLink.setBounds(1026, 871, 467, 32);
		contentPane.add(registrationPageLink);
		
		JLabel lblDate = new JLabel("Date:");
		lblDate.setForeground(new Color(255, 255, 153));
		lblDate.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblDate.setBounds(210, 290, 70, 56);
		contentPane.add(lblDate);
		
		dateField = new JTextField();
		dateField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		dateField.setColumns(10);
		dateField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		dateField.setBounds(281, 300, 317, 35);
		contentPane.add(dateField);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.BOLD, 40));
		lblNewLabel_1.setForeground(new Color(0, 0, 204));
		lblNewLabel_1.setBackground(new Color(255, 255, 153));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(0, 0, 1696, 120);
		contentPane.add(lblNewLabel_1);
		
		dividendRecordField = new JTextField();
		dividendRecordField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		dividendRecordField.setColumns(10);
		dividendRecordField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		dividendRecordField.setBounds(281, 544, 317, 38);
		contentPane.add(dividendRecordField);
		

		earningStabilityField = new JTextField();
		earningStabilityField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		earningStabilityField.setColumns(10);
		earningStabilityField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		earningStabilityField.setBounds(281, 494, 317, 38);
		contentPane.add(earningStabilityField);
		
		financialConditionField = new JTextField();
		financialConditionField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		financialConditionField.setColumns(10);
		financialConditionField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		financialConditionField.setBounds(281, 444, 317, 38);
		contentPane.add(financialConditionField);
		
		sizeField = new JTextField();
		sizeField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		sizeField.setColumns(10);
		sizeField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		sizeField.setBounds(281, 394, 317, 38);
		contentPane.add(sizeField);
		
		companyNameField = new JTextField();
		companyNameField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		companyNameField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		companyNameField.setColumns(10);
		companyNameField.setBounds(281, 347, 317, 35);
		contentPane.add(companyNameField);
		
		earningsGrowthInput = new JTextField();
		earningsGrowthInput.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		earningsGrowthInput.setColumns(10);
		earningsGrowthInput.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		earningsGrowthInput.setBounds(281, 594, 317, 38);
		contentPane.add(earningsGrowthInput);
		
		PEInput = new JTextField();
		PEInput.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		PEInput.setColumns(10);
		PEInput.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		PEInput.setBounds(281, 644, 317, 35);
		contentPane.add(PEInput);
		
		PBInput = new JTextField();
		PBInput.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		PBInput.setColumns(10);
		PBInput.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		PBInput.setBounds(281, 691, 317, 37);
		contentPane.add(PBInput);
		
		
		sScoreField = new JTextField();
		sScoreField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		sScoreField.setColumns(10);
		sScoreField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		sScoreField.setBounds(281, 740, 317, 37);
		contentPane.add(sScoreField);
		
		JTextField errorMessage = new JTextField("");
		errorMessage.setFont(new Font("Lucida Grande", Font.BOLD, 22));
		errorMessage.setForeground(new Color(200, 0, 0));
		errorMessage.setBorder(new LineBorder(new Color(0, 0, 204), 2, true));
		errorMessage.setBackground(new Color(0, 0, 204));
		errorMessage.setBounds(1101, 819, 241, 40);
		contentPane.add(errorMessage);
		
		
		JTextArea dateColumn = new JTextArea();
		dateColumn.setBackground(new Color(0, 0, 204));
		dateColumn.setForeground(new Color(255, 255, 153));
		dateColumn.setBounds(658, 232, 94, 629);
		contentPane.add(dateColumn);
		
		JTextArea nameColumn = new JTextArea();
		nameColumn.setBackground(new Color(0, 0, 204));
		nameColumn.setForeground(new Color(255, 255, 153));
		nameColumn.setBounds(760, 232, 156, 629);
		contentPane.add(nameColumn);
		
		JTextArea sizeColumn = new JTextArea();
		sizeColumn.setBackground(new Color(0, 0, 204));
		sizeColumn.setForeground(new Color(255, 255, 153));
		sizeColumn.setBounds(935, 232, 25, 629);
		contentPane.add(sizeColumn);
		
		JTextArea fcColumn = new JTextArea();
		fcColumn.setBackground(new Color(0, 0, 204));
		fcColumn.setForeground(new Color(255, 255, 153));
		fcColumn.setBounds(1026, 232, 32, 629);
		contentPane.add(fcColumn);
		
		JTextArea esColumn = new JTextArea();
		esColumn.setBackground(new Color(0, 0, 204));
		esColumn.setForeground(new Color(255, 255, 153));
		esColumn.setBounds(1156, 232, 32, 629);
		contentPane.add(esColumn);
		
		JTextArea drColumn = new JTextArea();
		drColumn.setBackground(new Color(0, 0, 204));
		drColumn.setForeground(new Color(255, 255, 153));
		drColumn.setBounds(1281, 232, 32, 629);
		contentPane.add(drColumn);
		
		JTextArea egColumn = new JTextArea();
		egColumn.setBackground(new Color(0, 0, 204));
		egColumn.setForeground(new Color(255, 255, 153));
		egColumn.setBounds(1402, 232, 48, 629);
		contentPane.add(egColumn);
		
		JTextArea peColumn = new JTextArea();
		peColumn.setBackground(new Color(0, 0, 204));
		peColumn.setForeground(new Color(255, 255, 153));
		peColumn.setBounds(1504, 232, 48, 629);
		contentPane.add(peColumn);
		
		JTextArea pbColumn = new JTextArea();
		pbColumn.setBackground(new Color(0, 0, 204));
		pbColumn.setForeground(new Color(255, 255, 153));
		pbColumn.setBounds(1564, 232, 48, 629);
		contentPane.add(pbColumn);
		
		JTextArea sscoreColumn = new JTextArea();
		sscoreColumn.setBackground(new Color(0, 0, 204));
		sscoreColumn.setForeground(new Color(255, 255, 153));
		sscoreColumn.setBounds(1653, 232, 37, 629);
		contentPane.add(sscoreColumn);
		
		
		JLabel lblsScore = new JLabel("S-Score:");
		lblsScore.setForeground(new Color(255, 255, 153));
		lblsScore.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblsScore.setBounds(176, 731, 105, 56);
		contentPane.add(lblsScore);
		
		JLabel sizelbl = new JLabel("Size:");
		sizelbl.setForeground(new Color(255, 255, 153));
		sizelbl.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		sizelbl.setBounds(217, 385, 105, 56);
		contentPane.add(sizelbl);
		
		JLabel lblFinancialCondition = new JLabel("Financial Condition:");
		lblFinancialCondition.setForeground(new Color(255, 255, 153));
		lblFinancialCondition.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblFinancialCondition.setBounds(39, 435, 241, 56);
		contentPane.add(lblFinancialCondition);
		
		JLabel lblEarningStability = new JLabel("Earning Stability:");
		lblEarningStability.setForeground(new Color(255, 255, 153));
		lblEarningStability.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblEarningStability.setBounds(71, 485, 209, 56);
		contentPane.add(lblEarningStability);
		
		JLabel lblDividendRecord = new JLabel("Dividend Record:");
		lblDividendRecord.setForeground(new Color(255, 255, 153));
		lblDividendRecord.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblDividendRecord.setBounds(71, 535, 217, 56);
		contentPane.add(lblDividendRecord);
		
		JLabel lblEarningsGrowth = new JLabel("Earnings Growth (%):");
		lblEarningsGrowth.setForeground(new Color(255, 255, 153));
		lblEarningsGrowth.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblEarningsGrowth.setBounds(23, 585, 256, 56);
		contentPane.add(lblEarningsGrowth);
		
		JLabel lblPe = new JLabel("P/E:");
		lblPe.setForeground(new Color(255, 255, 153));
		lblPe.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblPe.setBounds(224, 634, 56, 56);
		contentPane.add(lblPe);
		
		JLabel lblPb = new JLabel("P/B:");
		lblPb.setForeground(new Color(255, 255, 153));
		lblPb.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblPb.setBounds(226, 682, 62, 56);
		contentPane.add(lblPb);
		
		JLabel lblNeedHelpClick = new JLabel("Need help? click here!");
		lblNeedHelpClick.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNeedHelpClick.setForeground(new Color(255, 255, 153));
		lblNeedHelpClick.setFont(new Font("Lucida Grande", Font.PLAIN, 23));
		lblNeedHelpClick.setBounds(374, 124, 243, 32);
		lblNeedHelpClick.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e){
				 HelpDatabasePage hp = new HelpDatabasePage();
		         hp.setVisible(true);
			}
		});
		contentPane.add(lblNeedHelpClick);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSubmit.setForeground(new Color(0, 0, 204));
		btnSubmit.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnSubmit.setBackground(new Color(255, 255, 153));
		btnSubmit.setActionCommand("Store Results");
		btnSubmit.setBounds(322, 785, 241, 62);
		contentPane.add(btnSubmit);
		
		btnSubmit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
			ArrayList<String> filters = new ArrayList();{
				
				filters.add(" username = '" + user + "'");
				
			if(!dateField.getText().isEmpty()) {
				filters.add(" AND date " + dateField.getText().charAt(0) + "'" + dateField.getText().substring(1) + "'");
			}
			
			if(!companyNameField.getText().isEmpty()) {
				filters.add(" AND name = '" + companyNameField.getText()+"'");
			} 
			
			if(!sizeField.getText().isEmpty()) {
				filters.add(" AND size " + sizeField.getText());
			}
			
			if(!financialConditionField.getText().isEmpty()) {
				filters.add(" AND financialcondition " + financialConditionField.getText());
			}
			if(!earningStabilityField.getText().isEmpty()) {
				filters.add(" AND earningstability " + earningStabilityField.getText());
			}
			
			if(!dividendRecordField.getText().isEmpty()) {
				filters.add(" AND dividendrecord " + dividendRecordField.getText());
			}
			if(!earningsGrowthInput.getText().isEmpty()) {
				filters.add(" AND earningsgrowth " + earningsGrowthInput.getText());
			}
			
			if(!PEInput.getText().isEmpty()) {
				filters.add(" AND \"p/e\" " + PEInput.getText());
			}
			
			if(!PBInput.getText().isEmpty()) {
				filters.add(" AND \"p/b\" " + PBInput.getText());
			}
			
			if(!sScoreField.getText().isEmpty()) {
				filters.add(" AND \"s-score\" " + sScoreField.getText());
			}
			filter = filters;
			
			Client a = new Client ("127.0.0.1");
			try {
				a.filterResults();
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			}
				int z = 1;
				
				while (z == 1) {
					
					if (!dates.isEmpty()) {
						
						errorMessage.setText("");
						dateColumn.setText("");
						dateColumn.append("\n" + dates.get(0));
						dates.remove(0);
						
						nameColumn.setText("");
						nameColumn.append("\n" + name.get(0));
						name.remove(0);
						
						sizeColumn.setText("");
						sizeColumn.append("\n" + size.get(0));
						size.remove(0);
						
						fcColumn.setText("");
						fcColumn.append("\n" + fc.get(0));
						fc.remove(0);
						
						esColumn.setText("");
						esColumn.append("\n" + es.get(0));
						es.remove(0);
						
						drColumn.setText("");
						drColumn.append("\n" + dr.get(0));
						dr.remove(0);
						
						egColumn.setText("");
						if (eg.get(0).length() > 7) {
						egColumn.append("\n" + eg.get(0).substring(0, 6));
						eg.remove(0);
						} else {
							egColumn.append("\n" + eg.get(0));
							eg.remove(0);
						}
						
						peColumn.setText("");
						if(pe.get(0).length() > 7) {
						peColumn.append("\n" + pe.get(0).substring(0, 6));
						pe.remove(0);
						} else {
							peColumn.append("\n" + pe.get(0));
							pe.remove(0);
						}
						
						pbColumn.setText("");
						if (pb.get(0).length() > 7) {
						pbColumn.append("\n" + pb.get(0).substring(0, 6));
						pb.remove(0);
						} else {
							pbColumn.append("\n" + pb.get(0));
							pb.remove(0);
						}
						
						sscoreColumn.setText("");
						sscoreColumn.append("\n" + sScore.get(0));
						sScore.remove(0);
						
						
					} else {
						
						if (dateColumn.getText().isEmpty()) {
						errorMessage.setText("No results found!");
						}
						
						z = 2;
					}
				}
				
				
			}
			});

		JLabel lbl = new JLabel("");
		lbl.setBackground(new Color(0, 0, 204));
		lbl.setOpaque(true);
		lbl.setBounds(0, 0, 655, 936);
		contentPane.add(lbl);
		
		JLabel lblResults = new JLabel("RESULTS");
		lblResults.setForeground(new Color(255, 255, 153));
		lblResults.setFont(new Font("Lucida Grande", Font.BOLD, 33));
		lblResults.setBounds(1090, 124, 156, 56);
		contentPane.add(lblResults);
		
		JLabel resultsHeader = new JLabel("Date:                       Name:                          Size:       Financial Condition:       Earning Stability:       Dividend Record:       Earnings Growth(%):       P/E:          P/B:            S-Score:");
		resultsHeader.setFont(new Font("Lucida Grande", Font.BOLD, 11));
		resultsHeader.setForeground(new Color(255, 255, 153));
		resultsHeader.setBounds(667, 192, 1029, 40);
		contentPane.add(resultsHeader);
		

	}
}