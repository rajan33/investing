package investing;

import java.awt.BorderLayout;
import java.util.Date;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.awt.Cursor;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import java.awt.List;
import java.awt.Scrollbar;
import javax.swing.DefaultComboBoxModel;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

@SuppressWarnings("serial")
public class HelpDatabasePage extends JFrame {

	private JPanel contentPane;
	public static String[] details = new String[4];
	public static String user = "rs";
	public static int[] scrollerInputs;
	public static String[] userInputs;
	/**
	 * Launch the application.
	 */
	
	public static void main (String[] args) throws IOException {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
					
				try {
					HelpDatabasePage frame = new HelpDatabasePage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}
	
	

	/**
	 * Create the frame.
	 */
	public HelpDatabasePage() {
		setBounds(560, 200, 1696, 936); // frame size and position.
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 204));
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Date currentDate = new Date();
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = timeFormat.format(currentDate);
		JLabel dateLabel = new JLabel(date);
		dateLabel.setFont(new Font("Lucida Grande", Font.BOLD, 19));
		dateLabel.setForeground(new Color(0, 0, 205));
		dateLabel.setBounds(6, 6, 194, 40);
		contentPane.add(dateLabel);
		
		JLabel lblNameOfCompany = new JLabel("Name of Company: Simply enter the name of the company in the same way that you stored it.");
		lblNameOfCompany.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblNameOfCompany.setForeground(new Color(255, 255, 153));
		lblNameOfCompany.setBounds(20, 193, 1646, 49);
		contentPane.add(lblNameOfCompany);
		
		JLabel lblNewLabel_1 = new JLabel("HELP PAGE");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		lblNewLabel_1.setForeground(new Color(0, 0, 204));
		lblNewLabel_1.setBackground(new Color(255, 255, 153));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(0, 0, 1696, 120);
		contentPane.add(lblNewLabel_1);
		
		JLabel size = new JLabel("Size: Please use \"=\", \"<\", or \">\" before entering a score between 0 and 3.");
		size.setForeground(new Color(255, 255, 153));
		size.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		size.setBounds(20, 257, 766, 56);
		contentPane.add(size);
		
		JLabel lblFinancialCondition = new JLabel("Financial Condition: Please use \"=\", \"<\", or \">\" before entering a score between 0 and 3.");
		lblFinancialCondition.setForeground(new Color(255, 255, 153));
		lblFinancialCondition.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblFinancialCondition.setBounds(20, 325, 1340, 49);
		contentPane.add(lblFinancialCondition);
		
		JLabel lblEarningStability = new JLabel("Earning Stability: Please use \"=\", \"<\", or \">\" before entering a score between 0 and 10.");
		lblEarningStability.setForeground(new Color(255, 255, 153));
		lblEarningStability.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblEarningStability.setBounds(20, 386, 1110, 49);
		contentPane.add(lblEarningStability);
		
		JLabel lblDividendRecord = new JLabel("Dividend Record: Please use \"=\", \"<\", or \">\" before entering a score between 0 and 20.");
		lblDividendRecord.setForeground(new Color(255, 255, 153));
		lblDividendRecord.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblDividendRecord.setBounds(20, 447, 989, 56);
		contentPane.add(lblDividendRecord);
		
		JLabel lblEarningsGrowth = new JLabel("Earnings Growth (%): Please use \"=\", \"<\", or \">\" before entering a desired percentage growth.");
		lblEarningsGrowth.setForeground(new Color(255, 255, 153));
		lblEarningsGrowth.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblEarningsGrowth.setBounds(20, 515, 1615, 56);
		contentPane.add(lblEarningsGrowth);
		
		JLabel lblPe = new JLabel("P/E: Please use \"=\", \"<\", or \">\" before entering a desired P/E ratio.");
		lblPe.setForeground(new Color(255, 255, 153));
		lblPe.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblPe.setBounds(20, 583, 888, 56);
		contentPane.add(lblPe);
		
		JLabel lblPb = new JLabel("P/B: Please use \"=\", \"<\", or \">\" before entering a desired P/B ratio.");
		lblPb.setForeground(new Color(255, 255, 153));
		lblPb.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblPb.setBounds(20, 649, 998, 56);
		contentPane.add(lblPb);
		
		JTextPane txtpnSScoreGuidance = new JTextPane();
		txtpnSScoreGuidance.setBackground(new Color(0, 0, 204));
		txtpnSScoreGuidance.setForeground(new Color(255, 255, 153));
		txtpnSScoreGuidance.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 23));
		txtpnSScoreGuidance.setText("After editing the filters and selecting submit, the results will be showed on the right-hand side of the page. Please ensure that there are no spaces between any characters when editing the filters.");
		txtpnSScoreGuidance.setBounds(20, 808, 1615, 100);
		contentPane.add(txtpnSScoreGuidance);
		
		JLabel lblDatePleaseUs = new JLabel("Date: Please use \"=\", \"<\", or \">\" before entering the date in the format YYYY-MM-DD. ");
		lblDatePleaseUs.setForeground(new Color(255, 255, 153));
		lblDatePleaseUs.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblDatePleaseUs.setBounds(20, 132, 903, 49);
		contentPane.add(lblDatePleaseUs);
		
		JLabel lblSScore = new JLabel("S-Score: Please use \"=\", \"<\", or \">\" before entering a score between 0 and 9.");
		lblSScore.setForeground(new Color(255, 255, 153));
		lblSScore.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblSScore.setBounds(20, 718, 998, 56);
		contentPane.add(lblSScore);
		
	}
}