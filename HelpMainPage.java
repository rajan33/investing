package investing;

import java.awt.BorderLayout;
import java.util.Date;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.awt.Cursor;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import java.awt.List;
import java.awt.Scrollbar;
import javax.swing.DefaultComboBoxModel;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

@SuppressWarnings("serial")
public class HelpMainPage extends JFrame {

	private JPanel contentPane;
	public static String[] details = new String[4];
	public static String user = "rs";
	public static int[] scrollerInputs;
	public static String[] userInputs;
	/**
	 * Launch the application.
	 */
	
	public static void main (String[] args) throws IOException {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
					
				try {
					HelpMainPage frame = new HelpMainPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}
	
	

	/**
	 * Create the frame.
	 */
	public HelpMainPage() {
		setBounds(560, 200, 1696, 936); // frame size and position.
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 204));
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Date currentDate = new Date();
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = timeFormat.format(currentDate);
		JLabel dateLabel = new JLabel(date);
		dateLabel.setFont(new Font("Lucida Grande", Font.BOLD, 19));
		dateLabel.setForeground(new Color(0, 0, 205));
		dateLabel.setBounds(6, 6, 194, 40);
		contentPane.add(dateLabel);
		
		JLabel lblNameOfCompany = new JLabel("Name of Company: Please ensure to enter the name of the company you are evaluating in this field. Please avoid using any spaces before entering the name.");
		lblNameOfCompany.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblNameOfCompany.setForeground(new Color(255, 255, 153));
		lblNameOfCompany.setBounds(29, 155, 1646, 49);
		contentPane.add(lblNameOfCompany);
		
		JLabel lblNewLabel_1 = new JLabel("HELP PAGE");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		lblNewLabel_1.setForeground(new Color(0, 0, 204));
		lblNewLabel_1.setBackground(new Color(255, 255, 153));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(0, 0, 1696, 120);
		contentPane.add(lblNewLabel_1);
		
		JLabel size = new JLabel("Size: This is scored out of 3. The respective company gains 1 point for each of the following tests that it passes:");
		size.setForeground(new Color(255, 255, 153));
		size.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		size.setBounds(29, 205, 1179, 56);
		contentPane.add(size);
		
		JLabel lblFinancialCondition = new JLabel("Financial Condition: This is scored out of 3. The respective company gains 1 point for each of the following tests that it passes:");
		lblFinancialCondition.setForeground(new Color(255, 255, 153));
		lblFinancialCondition.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblFinancialCondition.setBounds(29, 292, 1340, 49);
		contentPane.add(lblFinancialCondition);
		
		JLabel lblEarningStability = new JLabel("Earning Stability: Out of the last 10 years, how many of them has the company made a net profit after tax.");
		lblEarningStability.setForeground(new Color(255, 255, 153));
		lblEarningStability.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblEarningStability.setBounds(29, 369, 1110, 49);
		contentPane.add(lblEarningStability);
		
		JLabel lblDividendRecord = new JLabel("Dividend Record: Out of the last 20 years, how many of them has the company paid dividends.");
		lblDividendRecord.setForeground(new Color(255, 255, 153));
		lblDividendRecord.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblDividendRecord.setBounds(29, 422, 989, 56);
		contentPane.add(lblDividendRecord);
		
		JLabel lblEarningsGrowth = new JLabel("Earnings Growth (%): This is measured as the percentage growth of the company over the last 10 years, using a 3 year average for the beginning and end.");
		lblEarningsGrowth.setForeground(new Color(255, 255, 153));
		lblEarningsGrowth.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblEarningsGrowth.setBounds(27, 481, 1615, 56);
		contentPane.add(lblEarningsGrowth);
		
		JLabel lblPe = new JLabel("P/E: This is the company's price to earnings ratio, as an average for the last 3 years.");
		lblPe.setForeground(new Color(255, 255, 153));
		lblPe.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblPe.setBounds(29, 544, 888, 56);
		contentPane.add(lblPe);
		
		JLabel lblPb = new JLabel("P/B: This is the company's price to book ratio. The book value is equal to stockholder's equity.");
		lblPb.setForeground(new Color(255, 255, 153));
		lblPb.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		lblPb.setBounds(29, 601, 998, 56);
		contentPane.add(lblPb);
		
		JLabel lblsizeScoring = new JLabel("1. minimum total market value of $2bn          2. minimum annual sales of $100m          3.minimum total assets of $50m");
		lblsizeScoring.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 16));
		lblsizeScoring.setForeground(new Color(255, 255, 255));
		lblsizeScoring.setBounds(145, 247, 989, 49);
		contentPane.add(lblsizeScoring);
		
		JLabel lblfinancialConditionScoring = new JLabel("1. Current assets are at least twice current liabilities         2. long-term debt does not exceed net current assets          3. debt does not exceed twice stock equity");
		lblfinancialConditionScoring.setForeground(Color.WHITE);
		lblfinancialConditionScoring.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 16));
		lblfinancialConditionScoring.setBounds(145, 329, 1313, 49);
		contentPane.add(lblfinancialConditionScoring);
		
		JTextPane txtpnSScoreGuidance = new JTextPane();
		txtpnSScoreGuidance.setBackground(new Color(0, 0, 204));
		txtpnSScoreGuidance.setForeground(new Color(255, 255, 153));
		txtpnSScoreGuidance.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 23));
		txtpnSScoreGuidance.setText("The S-Score is a score between 0 and 9. The higher the score, the better the investment appears to be. However, this is by no means a definitive answer. Instead, this tool should be used to help guide decisions and find companies which appear more appealling and suitable for further analysis. Once the result is showing, you can opt to store this result and review it at a later date.");
		txtpnSScoreGuidance.setBounds(29, 766, 1615, 142);
		contentPane.add(txtpnSScoreGuidance);
		
	}
}