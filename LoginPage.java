package investing;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.Cursor;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class LoginPage extends JFrame {

	private JPanel contentPane;
	private JTextField usernameField;

	private JTextField passwordField;
	public static String[] details = new String[2];
	public static String answer;

	/**
	 * Launch the application.
	 */
	
	public static void main (String[] args) throws IOException {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
					
				try {
					LoginPage frame = new LoginPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}
	
	

	/**
	 * Create the frame.
	 */
	public LoginPage() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(560, 200, 1696, 936); // frame size and position.
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 204));
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		
		JLabel welcomeBox = new JLabel("Welcome Back!");
		welcomeBox.setForeground(new Color(0, 0, 204));
		welcomeBox.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		welcomeBox.setHorizontalAlignment(SwingConstants.CENTER);
		welcomeBox.setOpaque(true);
		welcomeBox.setBackground(new Color(255, 255, 153));
		welcomeBox.setBounds(150, 338, 397, 308);
		contentPane.add(welcomeBox);

		
		JLabel registrationPageLink = new JLabel("Don't have an account? Click here to register!");
		registrationPageLink.setFont(new Font("Lucida Grande", Font.PLAIN, 23));
		registrationPageLink.setForeground(new Color(255, 255, 153));
		registrationPageLink.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e){
				 RegistrationPage rp = new RegistrationPage();
		         rp.setVisible(true);
				 dispose();
			}
		});
		registrationPageLink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		registrationPageLink.setBounds(81, 866, 591, 32);
		contentPane.add(registrationPageLink);
		
		JLabel lblNewLabel_1 = new JLabel("Login Here!");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		lblNewLabel_1.setForeground(new Color(0, 0, 204));
		lblNewLabel_1.setBackground(new Color(255, 255, 153));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(0, 0, 1696, 120);
		contentPane.add(lblNewLabel_1);
		
		JLabel lbl = new JLabel("");
		lbl.setBackground(new Color(0, 0, 204));
		lbl.setOpaque(true);
		lbl.setBounds(0, 0, 680, 936);
		contentPane.add(lbl);
		
		usernameField = new JTextField();
		usernameField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		usernameField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		usernameField.setColumns(10);
		usernameField.setBounds(679, 377, 802, 72);
		contentPane.add(usernameField);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		passwordField.setBorder(new LineBorder(new Color(255, 228, 181), 2, true));
		passwordField.setColumns(10);
		passwordField.setBounds(679, 533, 802, 72);
		contentPane.add(passwordField);
		
		JLabel username = new JLabel("Username:");
		username.setForeground(new Color(255, 255, 153));
		username.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		username.setBounds (679, 324, 334, 56);
		contentPane.add(username);
		
		JLabel password = new JLabel("Password:");
		password.setForeground(new Color(255, 255, 153));
		password.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		password.setBounds(679, 477, 334, 56);
		contentPane.add(password);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
				String[] detail = {usernameField.getText(), passwordField.getText()}; 
				details = detail;
				
				if(details[0].isEmpty() || details[1].isEmpty()) {
				
				usernameField.setText(null);
				passwordField.setText(null);
				JTextField errorMessage = new JTextField("You have not entered valid details. Please try again!");
				errorMessage.setFont(new Font("Lucida Grande", Font.BOLD, 13));
				errorMessage.setForeground(new Color(200, 0, 0));
				errorMessage.setBorder(new LineBorder(new Color(0, 0, 204), 2, true));
				errorMessage.setBackground(new Color(0, 0, 204));;
				errorMessage.setBounds(746, 870, 720, 40);
				contentPane.add(errorMessage);
				
				} else {
					
					Client a = new Client ("127.0.0.1");
					
					
					/*
					 * at this point, the login page should send the details to the server and the server then sends to the database to check the credentials, it will then provide a response.
					 * If it's rejected, an error message should show up saying incorrect details and also the connection should be closed.
					 * If accepted, then continue as normal and main page is run.
					 */
					
					
					try {
						a.login();
					} catch (UnknownHostException e1) {
						System.out.println("Couldn't find the host");
						e1.printStackTrace();
					} catch (IOException e1) {
						System.out.println("IOException occurred");
						e1.printStackTrace();
					} catch (InterruptedException e1) {
						System.out.println("interrupted exception");
						e1.printStackTrace();
					}
					
					int x = 1;
					
					while (x == 1) {
						if (answer == "login unsuccessful!" || answer == "login successful!") {
							x = 2;
						}	
					} 
					
					
					System.out.println(answer);
					if (answer == "login unsuccessful!") {
						JTextField errorMessage = new JTextField("You have entered incorrect details. Please try again");
						errorMessage.setFont(new Font("Lucida Grande", Font.BOLD, 13));
						errorMessage.setForeground(new Color(200, 0, 0));
						errorMessage.setBorder(new LineBorder(new Color(0, 0, 204), 2, true));
						errorMessage.setBackground(new Color(0, 0, 204));;
						errorMessage.setBounds(746, 870, 720, 40);
						contentPane.add(errorMessage);
					
					} else {
							MainPage.user = usernameField.getText();
							DatabasePage.user = usernameField.getText();
						    MainPage mp = new MainPage();
						    mp.setVisible(true);
						    dispose();
					}
					
					
					
				}
			}
		});
	
		btnLogin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnLogin.setForeground(new Color(0, 0, 0));
		btnLogin.setBackground(new Color(255, 255, 153));
		btnLogin.setBounds(934, 653, 285, 72);
		contentPane.add(btnLogin);
		
	
	}
	

	
	
	public static void setAnswer(String newAnswer) {
		answer = newAnswer;
	}
 	
}