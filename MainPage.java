package investing;

import java.awt.BorderLayout;
import java.util.Date;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.awt.Cursor;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import java.awt.List;
import java.awt.Scrollbar;
import javax.swing.DefaultComboBoxModel;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class MainPage extends JFrame {

	private JPanel contentPane;
	private JTextField companyNameField;
	public static String[] details = new String[4];
	private JTextField EarningsGrowthInput;
	private JTextField PEInput;
	private JTextField PBInput;
	public static String user = "rs50";
	public static int[] scrollerInputs;
	public static String[] userInputs;
	/**
	 * Launch the application.
	 */
	
	public static void main (String[] args) throws IOException {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
					
				try {
					MainPage frame = new MainPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}
	
	

	/**
	 * Create the frame.
	 */
	public MainPage() {
		
		Client a = new Client ("127.0.0.1");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(560, 200, 1696, 936); // frame size and position.
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 204));
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	
		
		JLabel registrationPageLink = new JLabel("Click here to look at your stored results");
		registrationPageLink.setFont(new Font("Lucida Grande", Font.PLAIN, 23));
		registrationPageLink.setForeground(new Color(255, 255, 153));
		registrationPageLink.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e){
				 DatabasePage dp = new DatabasePage();
		         dp.setVisible(true);
				 dispose();
			}
		});
		
		Date currentDate = new Date();
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = timeFormat.format(currentDate);
		JLabel dateLabel = new JLabel(date);
		dateLabel.setFont(new Font("Lucida Grande", Font.BOLD, 19));
		dateLabel.setForeground(new Color(0, 0, 205));
		dateLabel.setBounds(6, 6, 194, 40);
		contentPane.add(dateLabel);
		
		JLabel lblNewLabel = new JLabel("Logged in as: " + user);
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("Lucida Grande", Font.BOLD, 19));
		lblNewLabel.setForeground(new Color(0, 0, 205));
		lblNewLabel.setBounds(1225, 6, 450, 40);
		contentPane.add(lblNewLabel);
		
		JLabel email = new JLabel("Name of Company:");
		email.setForeground(new Color(255, 255, 153));
		email.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		email.setBounds (338, 242, 262, 56);
		contentPane.add(email);
		registrationPageLink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		registrationPageLink.setBounds(1208, 865, 467, 32);
		contentPane.add(registrationPageLink);
		
		JLabel lblNewLabel_1 = new JLabel("Please select/input the variables below");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		lblNewLabel_1.setForeground(new Color(0, 0, 204));
		lblNewLabel_1.setBackground(new Color(255, 255, 153));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(0, 0, 1696, 120);
		contentPane.add(lblNewLabel_1);
		
		companyNameField = new JTextField();
		companyNameField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		companyNameField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		companyNameField.setColumns(10);
		companyNameField.setBounds(612, 252, 317, 35);
		contentPane.add(companyNameField);
		
		JLabel lblScore_1 = new JLabel("");
		lblScore_1.setFont(new Font("Lucida Grande", Font.PLAIN, 70));
		lblScore_1.setForeground(new Color(0, 0, 204));
		lblScore_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblScore_1.setBounds(1310, 400, 149, 75);
		contentPane.add(lblScore_1);
		
		JLabel size = new JLabel("Size:");
		size.setForeground(new Color(255, 255, 153));
		size.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		size.setBounds(507, 310, 105, 56);
		contentPane.add(size);
		
		JLabel lblFinancialCondition = new JLabel("Financial Condition:");
		lblFinancialCondition.setForeground(new Color(255, 255, 153));
		lblFinancialCondition.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblFinancialCondition.setBounds(326, 363, 277, 56);
		contentPane.add(lblFinancialCondition);
		
		JLabel lblEarningStability = new JLabel("Earning Stability:");
		lblEarningStability.setForeground(new Color(255, 255, 153));
		lblEarningStability.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblEarningStability.setBounds(358, 419, 241, 56);
		contentPane.add(lblEarningStability);
		
		JLabel lblDividendRecord = new JLabel("Dividend Record:");
		lblDividendRecord.setForeground(new Color(255, 255, 153));
		lblDividendRecord.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblDividendRecord.setBounds(358, 476, 241, 56);
		contentPane.add(lblDividendRecord);
		
		JLabel lblEarningsGrowth = new JLabel("Earnings Growth (%):");
		lblEarningsGrowth.setForeground(new Color(255, 255, 153));
		lblEarningsGrowth.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblEarningsGrowth.setBounds(313, 530, 271, 56);
		contentPane.add(lblEarningsGrowth);
		
		JLabel lblPe = new JLabel("P/E:");
		lblPe.setForeground(new Color(255, 255, 153));
		lblPe.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblPe.setBounds(507, 594, 80, 56);
		contentPane.add(lblPe);
		
		JLabel lblPb = new JLabel("P/B:");
		lblPb.setForeground(new Color(255, 255, 153));
		lblPb.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		lblPb.setBounds(507, 652, 80, 56);
		contentPane.add(lblPb);
		
		JComboBox SizeInput = new JComboBox();
		SizeInput.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3"}));
		SizeInput.setBounds(612, 310, 93, 54);
		contentPane.add(SizeInput);
		
		JLabel lblNeedHelpClick = new JLabel("Need help? click here!");
		lblNeedHelpClick.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNeedHelpClick.setForeground(new Color(255, 255, 153));
		lblNeedHelpClick.setFont(new Font("Lucida Grande", Font.PLAIN, 23));
		lblNeedHelpClick.setBounds(1437, 130, 253, 32);
		contentPane.add(lblNeedHelpClick);
		lblNeedHelpClick.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e){
				 HelpMainPage hp = new HelpMainPage();
		         hp.setVisible(true);
			}
		});
		
		JComboBox FinancialConditionInput = new JComboBox();
		FinancialConditionInput.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3"}));
		FinancialConditionInput.setBounds(612, 365, 93, 54);
		contentPane.add(FinancialConditionInput);
		
		JComboBox EarningStabilityInput = new JComboBox();
		EarningStabilityInput.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		EarningStabilityInput.setBounds(612, 419, 93, 54);
		contentPane.add(EarningStabilityInput);
		
		JComboBox DividendRecordInput = new JComboBox();
		DividendRecordInput.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"}));
		DividendRecordInput.setBounds(612, 476, 93, 54);
		contentPane.add(DividendRecordInput);
		
		JTextField errorMessage = new JTextField("");
		errorMessage.setFont(new Font("Lucida Grande", Font.BOLD, 18));
		errorMessage.setForeground(new Color(200, 0, 0));
		errorMessage.setBorder(new LineBorder(new Color(0, 0, 204), 2, true));
		errorMessage.setBackground(new Color(0, 0, 204));
		errorMessage.setBounds(677, 810, 720, 40);
		contentPane.add(errorMessage);
		
		
		JButton btnStoreResult = new JButton("Store Result");
		btnStoreResult.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnStoreResult.setActionCommand("Store Results");
		btnStoreResult.setForeground(new Color(0, 0, 204));
		btnStoreResult.setBackground(new Color(255, 255, 153));
		btnStoreResult.setBounds(1256, 543, 241, 62);
		contentPane.add(btnStoreResult);
		
		btnStoreResult.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		btnStoreResult.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				
				int [] intInputs = {SizeInput.getSelectedIndex(), FinancialConditionInput.getSelectedIndex(), EarningStabilityInput.getSelectedIndex(), DividendRecordInput.getSelectedIndex()}; 
				
				
				if(companyNameField.getText().isEmpty() || EarningsGrowthInput.getText().isEmpty() || PEInput.getText().isEmpty() || PBInput.getText().isEmpty()) {
				
					errorMessage.setBounds(1050, 610, 720, 40);
					errorMessage.setFont(new Font("Lucida Grande", Font.BOLD, 18));
					errorMessage.setForeground(new Color(200, 0, 0));
					errorMessage.setBorder(new LineBorder(new Color(0, 0, 204), 2, true));
					errorMessage.setBackground(new Color(0, 0, 204));
					errorMessage.setText("Results could not be stored. Please make sure no inputs are empty.");
				
				} else {
					
					int [] inputs = {SizeInput.getSelectedIndex(), FinancialConditionInput.getSelectedIndex(), EarningStabilityInput.getSelectedIndex(), DividendRecordInput.getSelectedIndex()};
					scrollerInputs = inputs;
					double earningsGrowth = Double.parseDouble(EarningsGrowthInput.getText());
					
					double pe = Double.parseDouble(PEInput.getText());
					double pb = Double.parseDouble(PBInput.getText());
					
					double verdict =  1.00021 +(0.01064*inputs[0]) +(0.03041*inputs[1])+(0.01233*inputs[2])+(0.01152*inputs[3])+(0.04042 * earningsGrowth * earningsGrowth)+(-0.00489*pe * pb);
					
					int sScore;
					
					if (verdict > 300) {
						sScore = 9;
					} else if (verdict > 260) {
						sScore = 8;
					}else if (verdict > 220) {
						sScore = 7;
					}else if (verdict > 180) {
						sScore = 6;
					}else if (verdict > 140) {
						sScore = 5;
					}else if (verdict > 110) {
						sScore = 4;
					}else if (verdict > 80) {
						sScore = 3;
					}else if (verdict > 50) {
						sScore = 2;
					}else if (verdict > 0) {
						sScore = 1;
					} else {
						sScore = 0;
					}
					
					String result = Integer.toString(sScore);

					String[] stringInputs = {user, date, companyNameField.getText(), EarningsGrowthInput.getText(), PEInput.getText(),  PBInput.getText(), result};
					userInputs = stringInputs;
					
					
					try {
						a.storeDetails();
					} catch (UnknownHostException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					errorMessage.setText("Results have been stored!");
					errorMessage.setFont(new Font("Lucida Grande", Font.BOLD, 18));
					errorMessage.setForeground(new Color(0, 255, 0));
					errorMessage.setBounds(1250, 610, 720, 40);
					errorMessage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
				}
			}
		});
		
		
		JButton btnProduceSscore = new JButton("Produce S-Score");
		btnProduceSscore.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnProduceSscore.setForeground(new Color(0, 0, 204));
		btnProduceSscore.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnProduceSscore.setBackground(new Color(255, 255, 153));
		btnProduceSscore.setActionCommand("Store Results");
		btnProduceSscore.setBounds(644, 748, 241, 62);
		contentPane.add(btnProduceSscore);
		
		btnProduceSscore.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int [] inputs = {SizeInput.getSelectedIndex(), FinancialConditionInput.getSelectedIndex(), EarningStabilityInput.getSelectedIndex(), DividendRecordInput.getSelectedIndex()};
			
				
				
				if (companyNameField.getText().isEmpty() || EarningsGrowthInput.getText().isEmpty() || PEInput.getText().isEmpty() || PBInput.getText().isEmpty()) {
					errorMessage.setFont(new Font("Lucida Grande", Font.BOLD, 18));
					errorMessage.setForeground(new Color(200, 0, 0));
					errorMessage.setBorder(new LineBorder(new Color(0, 0, 204), 2, true));
					errorMessage.setBackground(new Color(0, 0, 204));
					errorMessage.setBounds(677, 810, 720, 40);
					errorMessage.setText("You have not entered valid details. Please try again");
					
				} else {
					
					try {
						
						double earningsGrowth = Double.parseDouble(EarningsGrowthInput.getText());
						
						double pe = Double.parseDouble(PEInput.getText());
						double pb = Double.parseDouble(PBInput.getText());
						
						double verdict =  1.00021 +(0.01064*inputs[0]) +(0.03041*inputs[1])+(0.01233*inputs[2])+(0.01152*inputs[3])+(0.04042 * earningsGrowth * earningsGrowth)+(-0.00489*pe * pb);
						
						int sScore;
						
						if (verdict > 300) {
							sScore = 9;
						} else if (verdict > 260) {
							sScore = 8;
						}else if (verdict > 220) {
							sScore = 7;
						}else if (verdict > 180) {
							sScore = 6;
						}else if (verdict > 140) {
							sScore = 5;
						}else if (verdict > 110) {
							sScore = 4;
						}else if (verdict > 80) {
							sScore = 3;
						}else if (verdict > 50) {
							sScore = 2;
						}else if (verdict > 0) {
							sScore = 1;
						} else {
							sScore = 0;
						}
						
						errorMessage.setText("");
						
						String result = Integer.toString(sScore);
						
						
						lblScore_1.setText(result);
						
					} catch (NumberFormatException e1) {
						errorMessage.setFont(new Font("Lucida Grande", Font.BOLD, 18));
						errorMessage.setForeground(new Color(200, 0, 0));
						errorMessage.setBorder(new LineBorder(new Color(0, 0, 204), 2, true));
						errorMessage.setBackground(new Color(0, 0, 204));
						errorMessage.setBounds(677, 810, 720, 40);
						errorMessage.setText("You have not entered valid details. Please try again");
					}
						
					
				}
			}
				
			});
		
		EarningsGrowthInput = new JTextField();
		EarningsGrowthInput.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		EarningsGrowthInput.setColumns(10);
		EarningsGrowthInput.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		EarningsGrowthInput.setBounds(612, 542, 317, 38);
		contentPane.add(EarningsGrowthInput);
		
		PEInput = new JTextField();
		PEInput.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		PEInput.setColumns(10);
		PEInput.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		PEInput.setBounds(612, 604, 317, 35);
		contentPane.add(PEInput);
		
		PBInput = new JTextField();
		PBInput.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		PBInput.setColumns(10);
		PBInput.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		PBInput.setBounds(612, 661, 317, 37);
		contentPane.add(PBInput);
		
		JLabel lbl = new JLabel("");
		lbl.setBackground(new Color(0, 0, 204));
		lbl.setOpaque(true);
		lbl.setBounds(0, 0, 680, 936);
		contentPane.add(lbl);
		
		JLabel welcomeBox = new JLabel("Result:");
		welcomeBox.setOpaque(true);
		welcomeBox.setVerticalAlignment(SwingConstants.TOP);
		welcomeBox.setForeground(new Color(0, 0, 204));
		welcomeBox.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		welcomeBox.setHorizontalAlignment(SwingConstants.CENTER);
		welcomeBox.setBackground(new Color(255, 255, 153));
		welcomeBox.setBounds(1218, 340, 317, 173);
		contentPane.add(welcomeBox);
		
	}
}