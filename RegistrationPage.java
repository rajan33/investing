package investing;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.awt.Cursor;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class RegistrationPage extends JFrame {

	private JPanel contentPane;
	private JTextField firstNameField;
	private JTextField usernameField;
	private JTextField passwordField;

	private JTextField confirmPasswordField;
	public static String[] details = new String[4];
	public static String outcome;
	private JTextField lastNameField;

	/**
	 * Launch the application.
	 */
	public static void main (String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistrationPage frame = new RegistrationPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistrationPage() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(560, 200, 1696, 936); // frame size and position.
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 204));
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel welcomeBox = new JLabel("Welcome!");
		welcomeBox.setForeground(new Color(0, 0, 204));
		welcomeBox.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		welcomeBox.setHorizontalAlignment(SwingConstants.CENTER);
		welcomeBox.setOpaque(true);
		welcomeBox.setBackground(new Color(255, 255, 153));
		welcomeBox.setBounds(150, 338, 397, 308);
		contentPane.add(welcomeBox);
		
		JLabel loginPageLink = new JLabel("Already registered? Click here to login!");
		loginPageLink.setFont(new Font("Lucida Grande", Font.PLAIN, 23));
		loginPageLink.setForeground(new Color(255, 255, 153));
		loginPageLink.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e){
			   LoginPage lp = new LoginPage();
			     lp.setVisible(true);
				 dispose();
			}
		});
		loginPageLink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		loginPageLink.setBounds(113, 873, 479, 32);
		contentPane.add(loginPageLink);
		
		JLabel lblNewLabel_1 = new JLabel("Register Here!");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		lblNewLabel_1.setForeground(new Color(0, 0, 204));
		lblNewLabel_1.setBackground(new Color(255, 255, 153));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(0, 0, 1696, 120);
		contentPane.add(lblNewLabel_1);
		
		JLabel lbl = new JLabel("");
		lbl.setBackground(new Color(0, 0, 204));
		lbl.setOpaque(true);
		lbl.setBounds(0, 0, 680, 936);
		contentPane.add(lbl);

		firstNameField = new JTextField();
		firstNameField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		firstNameField.setBorder(new LineBorder(new Color(255, 228, 181), 2, true));
		firstNameField.setBounds(746, 187, 802, 72);
		contentPane.add(firstNameField);
		firstNameField.setColumns(10);
		
		lastNameField = new JTextField();
		lastNameField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		lastNameField.setColumns(10);
		lastNameField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		lastNameField.setBounds(746, 315, 802, 72);
		contentPane.add(lastNameField);
		
		usernameField = new JTextField();
		usernameField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		usernameField.setBorder(new LineBorder(new Color(255, 222, 173), 2, true));
		usernameField.setColumns(10);
		usernameField.setBounds(746, 443, 802, 72);
		contentPane.add(usernameField);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		passwordField.setBorder(new LineBorder(new Color(255, 228, 181), 2, true));
		passwordField.setColumns(10);
		passwordField.setBounds(746, 571, 802, 72);
		contentPane.add(passwordField);
	
		
		confirmPasswordField = new JPasswordField();
		confirmPasswordField.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		confirmPasswordField.setBorder(new LineBorder(new Color(255, 228, 181), 2, true));
		confirmPasswordField.setColumns(10);
		confirmPasswordField.setBounds(746, 697, 802, 72);
		contentPane.add(confirmPasswordField);
		
		JLabel enterFirstName = new JLabel("Enter First Name:");
		enterFirstName.setForeground(new Color(255, 255, 153));
		enterFirstName.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		enterFirstName.setBounds(746, 132, 334, 56);
		contentPane.add(enterFirstName);
		
		JLabel username = new JLabel("Username:");
		username.setForeground(new Color(255, 255, 153));
		username.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		username.setBounds (746, 387, 334, 56);
		contentPane.add(username);
		
		JLabel enterPassword = new JLabel("Create Password:");
		enterPassword.setForeground(new Color(255, 255, 153));
		enterPassword.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		enterPassword.setBounds(746, 516, 334, 56);
		contentPane.add(enterPassword);
		
		JLabel confirmPassword = new JLabel("Confirm Password:");
		confirmPassword.setForeground(new Color(255, 255, 153));
		confirmPassword.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		confirmPassword.setBounds(746, 644, 334, 56);
		contentPane.add(confirmPassword);
		
		JLabel enterLastName = new JLabel("Last Name:");
		enterLastName.setForeground(new Color(255, 255, 153));
		enterLastName.setFont(new Font("Lucida Grande", Font.BOLD, 23));
		enterLastName.setBounds(746, 261, 334, 56);
		contentPane.add(enterLastName);
		
		JTextField Message = new JTextField("");
		Message.setFont(new Font("Lucida Grande", Font.BOLD, 18));
		Message.setForeground(new Color(200, 0, 0));
		Message.setBorder(new LineBorder(new Color(0, 0, 204), 2, true));
		Message.setBackground(new Color(0, 0, 204));;
		Message.setBounds(746, 870, 720, 40);
		contentPane.add(Message);
		
		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				
				String[] detail = {firstNameField.getText(), lastNameField.getText(), usernameField.getText(), passwordField.getText(), confirmPasswordField.getText()}; 
				
				details = detail;
				
				if(details[0].isEmpty() || details[1].isEmpty() || details[2].isEmpty() || details[3].isEmpty() || details[4].isEmpty() || !details[3].equals(details[4])) {
				firstNameField.setText(null);
				lastNameField.setText(null);
				usernameField.setText(null);
				passwordField.setText(null);
				confirmPasswordField.setText(null);
				Message.setText("You have not entered valid details. Please try again");
				Message.setForeground(new Color(200, 0, 0));
				
				
				} else {
					
					String answer = "success";
					outcome = answer;
					
					Client a = new Client ("127.0.0.1");
					
					try {
						a.register();
					} catch (UnknownHostException e1) {
						System.out.println(e1.getMessage());
						e1.printStackTrace();
					} catch (IOException e1) {
						System.out.println(e1.getMessage());
						e1.printStackTrace();
					}
					
					if (outcome.contains("failed")) {
						Message.setForeground(new Color(200, 0, 0));
						Message.setText("Username already taken. Please choose a new username.");
					} else if (outcome.contains("success")) {
					Message.setText("You have successfully signed up, Click here to login!");
					Message.setForeground(new Color(0, 255, 0));
					Message.setBounds(746, 870, 720, 40);
					Message.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
					Message.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
						
					    LoginPage lp = new LoginPage();
					    lp.setVisible(true);
					    dispose();
					}
					});
					}
				}
			}
		});
		
		btnConfirm.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnConfirm.setForeground(new Color(0, 0, 0));
		btnConfirm.setBackground(new Color(255, 255, 153));
		btnConfirm.setBounds(1000, 793, 285, 72);
		contentPane.add(btnConfirm);
		
	}
	
	public static void setOutcome(String newOutcome) {
		outcome = newOutcome;
	}
}
