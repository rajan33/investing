package regression;

import java.util.ArrayList;
import java.util.List;

public class Regression {
	
	public static void main (String[] args) {
	
List<Company> companies = new ArrayList<>();
	
	companies.add (new Company ("IBM", 3, 1, 10, 20, 118.2, 135.4, 9.6, 21.6, 0));
	companies.add (new Company ("Microsoft", 3, 2, 10, 11, 122.4, 101.2, 7.9, 53.9, 0));
	companies.add (new Company ("General Electric Company", 3, 2, 10, 20, 21, 12, 0, 44.8, 0));
	companies.add (new Company ("Coca-Cola Company", 3, 1, 10, 20, 115, 109.71,11.4, 56, 0));
	companies.add (new Company ("Walt Disney Company", 3, 1, 9, 19, 447, 22.5,11.3, 195.6, 1));
	companies.add (new Company ("McDonalds", 3, 1, 10, 20, 189, 90.3, 9.8, 50.7, 0));
	companies.add (new Company ("Apple inc.", 3, 3, 9, 7, 474.6, 169.8, 45.6, 269.9, 1));
	companies.add (new Company ("FedEx Corporation", 3, 2, 10, 7, 56.3, 46.7, 16.7, 103.2, 1));
	companies.add (new Company ("Hasbro", 3, 3, 8, 20, 100, 40.8, 4.2, 67.5, 0));
	companies.add (new Company ("Molson Coors Brewing Company", 3, 0, 10, 20, 61.4, 18.3, 0, 74.3, 0));
	companies.add (new Company ("Nike", 3, 3, 10, 20, 193, 64.8, 10.6, 192.1, 1));
	companies.add (new Company ("Sysco Corp.", 3, 1, 10, 20, 103, 62.1, 0, 47.8, 0));
	companies.add (new Company ("Morrison", 3, 1, 9, 16, 74.4, 24.9, 0, -38.1, 0));
	companies.add (new Company ("Rolls-Royce", 3, 1, 9, 17, 426.5, 45.8, 20.7, 71.9, 0));
	companies.add (new Company ("Verizon Communications", 3, 0, 8, 16, 28.5, 80.7, 0, -33, 0));
	companies.add (new Company ("BP", 3, 1, 9, 13, 807.7, 2241.2, 20.4, -3.8, 0));
	companies.add (new Company ("BT", 3, 1, 10, 15, 33.3, 1262.5, 0, -80.9, 0));
	companies.add (new Company ("Legal & General", 3, 1, 10, 7, 705.6, 467.1, 0, -27.8, 0));
	companies.add (new Company ("Marks & Spencer", 3, 2, 10, 7, 56.3, 3393.8, 0, -8.4, 0));
	companies.add (new Company ("Sage Group", 3, 0, 10, 7, 553.3, 965251.5, 23.9, -68.7, 0));
	companies.add (new Company ("Sainsbury", 3, 1, 10, 16, 26.8, 22.9, 13, -26.6, 0));
	companies.add (new Company ("Severn Trent", 3, 0, 9, 17, -26.3, 45.4, 14.2, 89.4, 0));
	companies.add (new Company ("Unilever", 3, 0, 10, 18, 335.9, 38.4, 2.4, 35.6, 0));
	companies.add (new Company ("Vodafone", 3, 1, 4, 17, 100, 11.3, 75.5, 57.4, 0));
	companies.add (new Company ("The Boeing Company", 3, 0, 9, 20, -32.2, 126.8, 2, 13.5, 0));
	companies.add (new Company ("Exxon Corporation", 3, 1, 10, 6, 29.5, 142.2, 17.3, 17.2, 0));
	companies.add (new Company ("International Paper Company", 3, 1, 10, 20, -82.4, 460.5, 0, -29.6, 0));
	companies.add (new Company ("Johnson & Johnson", 3, 2, 10, 20, 225, 316, 14.5, 29.8, 0));
	companies.add (new Company ("The Procter & Gamble Company", 3, 1, 9, 20, 104, 583.4, 9, -4, 0));
	companies.add (new Company ("Wal-Mart Inc.", 3, 1, 10, 20, 180, 761.2, 17, -21.2, 0));
	companies.add (new Company ("Best Buy Co. Inc.", 3, 2, 10, 0, 670, 1027.7, 32.3, 42, 0));
	companies.add (new Company ("Brown-Forman Corp.", 3, 2, 10, 20, 79, 117.4, 5.3, 64.9, 0));
	companies.add (new Company ("Emerson Electric Company", 3, 1, 10, 20, 95, 99.9, 0, 7.8, 0));
	companies.add (new Company ("Target Corp.", 3, 1, 10, 17, 88.8, 248.2, 15.8, 45.2, 0));
	companies.add (new Company ("Tesco", 3, 1, 10, 6, 51.9, 6089.8, 10.3, 75.4, 0));
	companies.add (new Company ("PulteGroup Inc.", 3, 3, 10, 15, 188, 7, 33.8, 516.5, 1));
	companies.add (new Company ("United Technologies Corporation",3, 0, 8, 20, 100, 139.5, 29, 56.3, 0));
	companies.add (new Company ("Pfizer Inc.",3, 1, 10, 20, 10.1, 25.4, 12.7, 74.8, 0));
	companies.add (new Company ("Amgen Inc.", 3, 3, 10, 0, 573.1, 1697.5, 0, -7.4, 0));
	companies.add (new Company ("Oracle Corp.", 3, 3, 10, 1, 56.6, 97.5, 17.3, 69.7, 0));
	companies.add (new Company ("Kingfisher", 3, 1, 10, 6, 132.4, 98.6, 0, -53, 0));
	companies.add (new Company ("Pearson", 3, 1, 8, 17, 100, 34.1, 5.3, 36.4, 0));
	
	

	}

}
