package investing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Server {
	
	/**
	 * This class deals with all of the functions that require interaction with the database. 
	 * Differing methods are appropriate for each different action required,
	 * 
	 */
	
	public static void main (String[] args) throws IOException {
	
				registerDetails();
				attemptLogin();
				storeResults();
				findResults();
	}
	 
	public static void registerDetails() {
		try {

			ServerSocket ss = new ServerSocket(6000);
			Socket s = ss.accept();
			
			InputStreamReader i = new InputStreamReader(s.getInputStream());
			BufferedReader br = new BufferedReader(i);
			
			int x = 1;
			int z = 1;
			
			ArrayList<String> registration = new ArrayList<>();
			
			while (x == 1) {
			String str = br.readLine();
			registration.add(str);
			
			if (registration.size() ==  4) {
				x = 2;	
			}
			
			}
			
			Class.forName("org.postgresql.Driver");
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5555/postgres", "postgres", "Gerrard8");
			
			PreparedStatement st = con.prepareStatement("SELECT username FROM public.\"Users\" WHERE username = '" + registration.get(2) + "';");
			ResultSet rs = st.executeQuery();
			PrintWriter p = new PrintWriter(s.getOutputStream());
			
			if (rs.next()) {
				p.println("failed");
			} else {
				z = 2;
				p.println("success");
				System.out.println(p.toString());
			}
			p.flush();
			
			if (z == 2) {
			PreparedStatement st1 = con.prepareStatement("INSERT INTO public.\"Users\"(username, firstname, lastname, pass) VALUES ('" + registration.get(2)+ "','" + registration.get(0) + "','" + registration.get(1) + "','" + registration.get(3) + "');");
			ResultSet rs1 = st1.executeQuery();
			}
			
			ss.close();
			s.close();
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		}
	
	public static void attemptLogin() {
		try {
			
			ServerSocket ss = new ServerSocket(5002);
			Socket s = ss.accept();
			
			InputStreamReader i = new InputStreamReader(s.getInputStream());
			BufferedReader br = new BufferedReader(i);
			
			
			
			int x = 1;
			
			ArrayList<String> login = new ArrayList<>();
			
			while (x == 1) {
			String str = br.readLine();
			login.add(str);
			
			if (login.size() ==  2) {
				x = 2;	
			}
			
			}

			Class.forName("org.postgresql.Driver");
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5555/postgres", "postgres", "Gerrard8");
			
			PreparedStatement st = con.prepareStatement("SELECT username, pass FROM public.\"Users\" WHERE username = '" + login.get(0) + "' AND pass = '" + login.get(1) + "';");
			ResultSet rs = st.executeQuery();
			
			PrintWriter p = new PrintWriter(s.getOutputStream());
			
			if (rs.next()) {
				p.println("success");
			} else {
				p.println("failed");
			}
			p.flush();
			
			ss.close();
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		
		
		}

	public static void storeResults() {
		try {

			ServerSocket ss = new ServerSocket(5005);
			Socket s = ss.accept();
			
			System.out.println("Client connected");
			
			InputStreamReader i = new InputStreamReader(s.getInputStream());
			BufferedReader br = new BufferedReader(i);
			
			int x = 1;
			
			ArrayList<String> results = new ArrayList<>();
			
			while (x == 1) {
			String str = br.readLine();
			results.add(str);
			System.out.println(results);
			
			if (results.size() ==  11) {
				x = 2;	
			}
			
			}
			System.out.println("results details complete");
			
			PrintWriter p = new PrintWriter(s.getOutputStream());
			
			Class.forName("org.postgresql.Driver");
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5555/postgres", "postgres", "Gerrard8");
			PreparedStatement st = con.prepareStatement("INSERT INTO public.\"Results\"(\n" + 
					"	username, date, name, size, financialcondition, earningstability, dividendrecord, earningsgrowth, \"p/e\", \"p/b\", \"s-score\")\n" + 
					"	VALUES ('" + results.get(0) + "','" + results.get(1) + "','" + results.get(2) + "','" + results.get(3) + "','" + results.get(4) + "','" + results.get(5) + "','" + results.get(6) + "','" + results.get(7) + "','" + results.get(8) + "','" + results.get(9) + "','" + results.get(10) + "');");
			ResultSet rs = st.executeQuery();
			/*while(rs.next()) {
				System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4)); 
			}
		*/
			
			ss.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		}
	
	public static void findResults() {
		try {

			ServerSocket ss = new ServerSocket(5006);
			Socket s = ss.accept();
			
			System.out.println("Client connected");
			
			InputStreamReader i = new InputStreamReader(s.getInputStream());
			BufferedReader br = new BufferedReader(i);
			
			int x = 1;
			
			ArrayList<String> filters = new ArrayList<>();
			
			while (x == 1) {
			String str = br.readLine();
			if(str.contains("stop")) {
				x = 2;	
			} else {
			filters.add(str);
			}	
			}

			String initialStatement = "SELECT date, name, size, financialcondition, earningstability, dividendrecord, earningsgrowth, \"p/e\", \"p/b\", \"s-score\"\n	FROM public.\"Results\" WHERE";
			StringBuffer sb = new StringBuffer();
			sb.append(initialStatement);
			
			for(int y=0; y < filters.size(); y++) {
				sb.append(filters.get(y));	
			}
			
			PrintWriter p = new PrintWriter(s.getOutputStream());
			
			Class.forName("org.postgresql.Driver");
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5555/postgres", "postgres", "Gerrard8");
			PreparedStatement st = con.prepareStatement(sb +  ";");
			ResultSet rs = st.executeQuery();
			
			while(rs.next()) {
				
				p.println(rs.getString(1));
				p.flush();
				p.println(rs.getString(2));
				p.flush();
				p.println(rs.getString(3));
				p.flush();
				p.println(rs.getString(4));
				p.flush();
				p.println(rs.getString(5));
				p.flush();
				p.println(rs.getString(6));
				p.flush();
				p.println(rs.getString(7));
				p.flush();
				p.println(rs.getString(8));
				p.flush();
				p.println(rs.getString(9));
				p.flush();
				p.println(rs.getString(10));
				p.flush();
				
				//p.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7) + " " + rs.getString(8) + " " + rs.getString(9) + " " + rs.getString(10));
				//p.flush();
			}
				p.println("All results found");
				p.flush();
		
				ss.close();
				
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		}

}
