package regression;

import java.util.ArrayList;
import java.util.List;

public class TestSet {
	
	public static void main (String[] args) {
		List<Company> testCompanies = new ArrayList<>();
		
		testCompanies.add (new Company ("Caterpillar Inc.",3, 0, 10, 20, 136.3, 47.7, 0, 70.1, 0));
		testCompanies.add (new Company ("Chevron Corporation", 3, 2, 10, 20, 122.5, 14.2, 0, 14.5, 0));
		testCompanies.add (new Company ("Merck & Co., Inc.", 3, 2, 9, 20, -44, 70.7, 0, 31.8, 0));
		testCompanies.add (new Company ("L Brands Inc.", 3, 0, 10, 20, 71.7, 1853.3, 12.5, 13.5, 0));
		testCompanies.add (new Company ("Northrop Grumman", 3, 1, 9, 20, 148.6, 15.5, 11.4, 359.5, 1));
		testCompanies.add (new Company ("The Clorox Company", 3, 0, 10, 20, 13.4, 1836.9, 3.4, 92.6, 0));
		testCompanies.add (new Company ("National Grid", 3, 0, 10, 17, 38, 47.9, 24.3, 12.4, 0));
		testCompanies.add (new Company ("Rio Tinto", 3, 1, 9, 20, 67.5, 44.9, 0, 20.2, 0));
		testCompanies.add (new Company ("Whitbread", 3, 1, 10, 20, 46.9, 47.2, 11.9, 58.5, 0));
		testCompanies.add (new Company ("PulteGroup Inc.", 3, 3, 10, 15, 188, 7, 33.8, 516.5, 1));
		
		for(Company c : testCompanies) {
			
			double verdict =  1.00021 +(0.01064*c.getSize()) +(0.03041*c.getFinancialCondition())+(0.01233*c.getEarningStability())+(0.01152*c.getDividendRecord())+(0.04042*c.getEarningsGrowth()*c.getEarningsGrowth())+(-0.00489*c.getPepb());
			
			System.out.println(verdict);
			
			int sScore;
			
			if (verdict > 300) {
				sScore = 9;
			} else if (verdict > 260) {
				sScore = 8;
			}else if (verdict > 220) {
				sScore = 7;
			}else if (verdict > 180) {
				sScore = 6;
			}else if (verdict > 140) {
				sScore = 5;
			}else if (verdict > 110) {
				sScore = 4;
			}else if (verdict > 80) {
				sScore = 3;
			}else if (verdict > 50) {
				sScore = 2;
			}else if (verdict > 0) {
				sScore = 1;
			} else {
				sScore = 0;
			}
			
			System.out.println("The S-Score for " + c.getName() + " is " + sScore +". This company went on to have a later price increase of " + c.getPriceChange() + "%");
		
	}

}
}
